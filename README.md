# Repository for Networking Fundamentals for Cybersecurity Professionals

This is the repository for the Networking Fundamentals for Cybersecurity Professionals video series. I will be putting slides, notes or any other supplementary material into this repository.

Please check out my [channel](https://www.youtube.com/channel/UCbVhtw4KJoXfVcfWXgWfnEA) for the videos.
